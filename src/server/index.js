const fs = require("fs");
const { normalize } = require("path");

// 1..Number of matches played per year for all the years in IPL.

const dataMatches = fs.readFileSync("../data/matches.csv", {
  encoding: "utf-8",
});
// console.log(dataMatches);

const convertToJson = (data) => {
  const lines = data.split("\n");
  const keys = lines[0].split(",");

  const restData = lines.slice(1).filter((line) => line.length);

  const jsonArr = restData.map((eachLine) => {
    const values = eachLine.split(",");
    const jsonObj = {};
    values.forEach((eachValue, index) => {
      jsonObj[keys[index]] = eachValue;
    });
    return jsonObj;
  });
  return jsonArr;
};

const convertedDataMatches = convertToJson(dataMatches);
// console.log(convertedDataMatches);

const matchesPerSeason = (result) => {
  let resultObj = {};
  result.forEach((item) => {
    if (resultObj[item.season]) {
      resultObj[item.season]++;
    } else {
      resultObj[item.season] = 1;
    }
    // resultObj[item.season] = resultObj[item.season]++ || 1;
  });
  return resultObj;
};

const outputDataForQ1 = matchesPerSeason(convertedDataMatches);
console.log(outputDataForQ1);

// fs.writeFileSync(
//   "/home/abhijeet/Desktop/Cohort 21/IPL/src/public/output/matchesPerSeason.json",
//   JSON.stringify(outputDataForQ1),
//   {
//     encoding: "utf-8",
//   }
// );

// 2..Number of matches won per team per year in IPL.

const matchesWonPerTeamPerYear = (result) => {
  return result.reduce((acc, curr) => {
    if (acc[curr.season]) {
      if (acc[curr.season][curr.winner]) {
        acc[curr.season][curr.winner]++;
      } else {
        acc[curr.season][curr.winner] = 1;
      }
    } else {
      acc[curr.season] = {};
      acc[curr.season][curr.winner] = 1;
    }
    return acc;
  }, {});
};

const outputDataForQ2 = matchesWonPerTeamPerYear(convertedDataMatches);

// fs.writeFileSync(
//   "/home/abhijeet/Desktop/Cohort 21/IPL/src/public/output/matchesWonPerTeamPerYear.json",
//   JSON.stringify(outputDataForQ2),
//   {
//     encoding: "utf-8",
//   }
// );

//

const dataDeliveries = fs.readFileSync("../data/deliveries.csv", {
  encoding: "utf-8",
});

const convertedDataDeliveries = convertToJson(dataDeliveries);

let getIdsOf2016 = (data) => {
  let array = [];
  for (let item of data) {
    if (item.season === "2016") {
      array.push(item.id);
    }
  }
  return array;
};
const arrayOfIds2016 = getIdsOf2016(convertedDataMatches);

const extraRunsConceded = convertedDataDeliveries.reduce((acc, curr) => {
  if (arrayOfIds2016.includes(curr.match_id)) {
    if (acc[curr.bowling_team]) {
      acc[curr.bowling_team] += parseInt(curr.extra_runs);
    } else {
      acc[curr.bowling_team] = parseInt(curr.extra_runs);
    }
  }
  return acc;
}, {});

// fs.writeFileSync(
//   "/home/abhijeet/Desktop/Cohort 21/IPL/src/public/output/extraRunsConceded.json",
//   JSON.stringify(extraRunsConceded),
//   {
//     encoding: "utf-8",
//   }
// );

// 4..Top 10 economical bowlers in the year 2015

let getIdsOf2015 = (data) => {
  let array = [];

  for (let item of data) {
    if (item.season === "2015") {
      array.push(item.id);
    }
  }
  return array;
};

const arrayOfIds2015 = getIdsOf2015(convertedDataMatches);

const totalDeliveries2015 = convertedDataDeliveries.filter(
  (item) =>
    item.match_id >= +arrayOfIds2015[0] &&
    item.match_id <= +arrayOfIds2015[arrayOfIds2015.length - 1]
);

const bowlersOf2015 = (deliveries) => {
  return deliveries.reduce((acc, curr) => {
    if (acc[curr.bowler]) {
      acc[curr.bowler].balls++;
      acc[curr.bowler].runs += +curr.total_runs;
    } else {
      acc[curr.bowler] = { balls: 1, runs: +curr.total_runs };
    }
    return acc;
  }, {});
};

const allBowlersData = bowlersOf2015(totalDeliveries2015);

const findEconomy = (bowlers) => {
  // {bName, economy};
  let economy = (balls, runs) => ((runs / balls) * 6).toFixed();
  let result = [];
  Object.keys(bowlers).forEach((i) => {
    result.push({
      bowlerName: i,
      economy: economy(bowlers[i].balls, bowlers[i].runs),
    });
  });
  return result;
};

const bowlersNameWithEconomy = findEconomy(allBowlersData);

let top10EconomicBowlers = bowlersNameWithEconomy
  .sort((a, b) => a.economy - b.economy)
  .slice(0, 10);
// console.log(top10EconomicBowlers);

// fs.writeFileSync(
//   "/home/abhijeet/Desktop/Cohort 21/IPL/src/public/output/top10Bowlers.json",
//   JSON.stringify(top10EconomicBowlers),
//   {
//     encoding: "utf-8",
//   }
// );
