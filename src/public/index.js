const fetchData = () => {
  fetch("./output/matchesPerSeason.json")
    .then((result) => result.json())
    .then((result) => {
      console.log(result);
      generateChart(result);
    });
};

function generateChart(data) {
  const transformData = Object.keys(data).map((item) => {
    return [item, data[item]];
  });
  Highcharts.chart("container", {
    chart: {
      type: "column",
    },
    title: {
      align: "left",
      text: "1. Number of matches played per year for all the years in IPL.",
    },
    accessibility: {
      announceNewData: {
        enabled: true,
      },
    },
    xAxis: {
      type: "category",
    },
    yAxis: {
      title: {
        text: "No of Matches",
      },
    },
    legend: {
      enabled: false,
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          // format: "{point.y:.1f}%",
        },
      },
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>',
    },

    series: [
      {
        name: "No of matches in year",
        colorByPoint: true,
        data: transformData,
      },
    ],
    drilldown: {
      breadcrumbs: {
        position: {
          align: "right",
        },
      },
    },
  });
}
let output = fetchData();

const fetchDataMatchesWon = () => {
  fetch("./output/matchesWonPerTeamPerYear.json")
    .then((result) => result.json())
    .then((result) => {
      console.log(result);
      generateChartMatchesWon(result);
    });
};

// {
//   "Kolkata Knight Riders": 6,
//   "Chennai Super Kings": 9,
//   "Delhi Daredevils": 7,
//   "Royal Challengers Bangalore": 4,
//   "Rajasthan Royals": 13,
//   "Kings XI Punjab": 10,
//   "Deccan Chargers": 2,
//   "Mumbai Indians": 7
// }

function generateChartMatchesWon(data) {
  const yearsxAxis = Object.keys(data);
  let allTeams = {};
  Object.values(data).forEach((curr) => {
    Object.keys(curr).forEach((x) => {
      if (!allTeams[x]) {
        allTeams[x] = [];
      }
    });
  });
  Object.values(data).forEach((item) => {
    Object.keys(allTeams).forEach((x) => {
      allTeams[x].push(item[x] || 0);
    });
  });

  const finalyAxis = Object.keys(allTeams).reduce((acc, curr) => {
    let obj = {};
    obj.name = curr;
    obj.data = allTeams[curr];
    acc.push(obj);
    return acc;
  }, []);

  // console.log(final);
  // console.log(finalyAxis);
  Highcharts.chart("container4", {
    chart: {
      type: "column",
    },
    title: {
      align: "left",
      text: "2. Number of matches won per team per year in IPL.",
    },
    xAxis: {
      categories: yearsxAxis,
      crosshair: true,
    },
    yAxis: {
      min: 0,
      title: {
        text: "No of Matches",
      },
    },
    tooltip: {
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
      pointFormat:
        '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
      footerFormat: "</table>",
      shared: true,
      useHTML: true,
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0,
      },
      // EasygoingUselessDataset,
    },
    series: finalyAxis,
  });
}
let output4 = fetchDataMatchesWon();

const fetchDataExtraRun = () => {
  fetch("./output/extraRunsConceded.json")
    .then((result) => result.json())
    .then((result) => {
      console.log(result);
      generateChartExtraRun(result);
    });
};

function generateChartExtraRun(data) {
  const transformData = Object.keys(data).map((item) => {
    return [item, data[item]];
  });

  // console.log(transformData);
  Highcharts.chart("container2", {
    chart: {
      type: "column",
    },
    title: {
      align: "left",
      text: "3.. Extra runs conceded per team in the year 2016",
    },
    accessibility: {
      announceNewData: {
        enabled: true,
      },
    },
    xAxis: {
      type: "category",
    },
    yAxis: {
      title: {
        text: "Extra Runs",
      },
    },
    legend: {
      enabled: false,
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          // format: "{point.y:.1f}%",
        },
      },
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> runs<br/>',
    },

    series: [
      {
        name: "Runs",
        colorByPoint: true,
        data: transformData,
      },
    ],
    drilldown: {
      breadcrumbs: {
        position: {
          align: "right",
        },
      },
    },
  });
}
let outputExtraRun = fetchDataExtraRun();

const fetchDataTop10Bowlers = () => {
  fetch("./output/top10Bowlers.json")
    .then((result) => result.json())
    .then((result) => {
      console.log(result);
      generateChrtTop10Bowlers(result);
    });
};

function generateChrtTop10Bowlers(data) {
  const transformData = data.map((item) => {
    return [item.bowlerName, parseInt(item.economy)];
  });

  Highcharts.chart("container3", {
    chart: {
      type: "column",
    },
    title: {
      align: "left",
      text: "4. Top 10 economical bowlers in the year 2015",
    },
    accessibility: {
      announceNewData: {
        enabled: true,
      },
    },
    xAxis: {
      type: "category",
    },
    yAxis: {
      title: {
        text: "Bowler's Economy",
      },
    },
    legend: {
      enabled: false,
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          // format: "{point.y:.1f}%",
        },
      },
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat:
        '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>',
    },

    series: [
      {
        name: "Economy",
        colorByPoint: true,
        data: transformData,
      },
    ],
    drilldown: {
      breadcrumbs: {
        position: {
          align: "right",
        },
      },
    },
  });
}
let outputTop10Bowlers = fetchDataTop10Bowlers();
